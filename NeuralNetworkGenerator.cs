﻿//Ethan Alexander Shulman 2017

using System;
using System.Threading;

namespace EthansNeuralNetwork
{

    /// <summary>
    /// Used for generating input deltas from a desired target output.
    /// </summary>
    public class NeuralNetworkGenerator
    {

        private bool hasRecurring;

        private NeuralNetwork neuralNetwork;

        private NeuralNetworkContext runtimeContext;
        private NeuralNetworkContext[] stackedRuntimeContext;
        private NeuralNetworkFullContext fullContext;
        private NeuralNetworkFullContext[] stackedFullContext;
        private NeuralNetworkPropagationState derivativeMemory;
        private NeuralNetworkPropagationState[] stackedDerivativeMemory;
        private NeuralNetworkDerivativeMemory derivatives = new NeuralNetworkDerivativeMemory();
        private float[][] recurringBPBuffer;

        private Thread thread;
        private int maxUnrollLength;

        /// <summary>
        /// Create new NeuralNetworkGenerator.
        /// </summary>
        /// <param name="nn">NeuralNetwork to train.</param>
        public NeuralNetworkGenerator(NeuralNetwork nn, int maxUnrollLen)
        {
            neuralNetwork = nn;
            maxUnrollLength = maxUnrollLen;

            //check for recurring layer, if need to stack and unroll
            if (nn.outputLayer.recurring)
            {
                hasRecurring = true;
            }
            else
            {
                for (int i = 0; i < nn.hiddenLayers.Length; i++)
                {
                    if (nn.hiddenLayers[i].recurring)
                    {
                        hasRecurring = true;
                        break;
                    }
                }
            }

            derivatives.Setup(nn);
            if (hasRecurring)
            {
                recurringBPBuffer = new float[nn.hiddenLayers.Length][];
                for (int i = 0; i < recurringBPBuffer.Length - 1; i++)
                {
                    if (nn.hiddenLayers[i].recurring) recurringBPBuffer[i] = new float[nn.hiddenLayers[i].numberOfNeurons];
                }

                stackedRuntimeContext = new NeuralNetworkContext[maxUnrollLength];
                stackedFullContext = new NeuralNetworkFullContext[maxUnrollLength];
                stackedDerivativeMemory = new NeuralNetworkPropagationState[maxUnrollLength];
                for (int i = 0; i < maxUnrollLength; i++)
                {
                    stackedRuntimeContext[i] = new NeuralNetworkContext();
                    stackedRuntimeContext[i].Setup(nn);

                    stackedFullContext[i] = new NeuralNetworkFullContext();
                    stackedFullContext[i].Setup(nn);

                    stackedDerivativeMemory[i] = new NeuralNetworkPropagationState();
                    stackedDerivativeMemory[i].Setup(nn, stackedRuntimeContext[i], stackedFullContext[i], derivatives);
                    stackedDerivativeMemory[i].inputMem = new float[nn.inputLayer.numberOfNeurons];
                }
            }
            else
            {
                runtimeContext = new NeuralNetworkContext();
                runtimeContext.Setup(nn);

                fullContext = new NeuralNetworkFullContext();
                fullContext.Setup(nn);

                derivativeMemory = new NeuralNetworkPropagationState();
                derivativeMemory.Setup(nn, runtimeContext, fullContext, derivatives);
                derivativeMemory.inputMem = new float[nn.inputLayer.numberOfNeurons];
            }
        }


        /// <summary>
        /// Generate input deltas for a recurring network.
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="targetData"></param>
        /// <param name="crossEntropy"></param>
        /// <returns>Array of input deltas.</returns>
        public float[][] InputErrorPropagationRecurring(float[][] inputData, float[][] targetData)
        {
            if (inputData.Length > maxUnrollLength)
            {
                throw new System.ArgumentException("Input/target array cannot be larger then max unroll length!");
            }
            if (!hasRecurring)
            {
                throw new System.ArgumentException("No recurring layers to perform recurring error propagation on.");
            }

            derivatives.Reset();

            for (int i = 0; i < inputData.Length; i++)
            {
                stackedRuntimeContext[i].Reset(true);
                stackedDerivativeMemory[i].Reset();
                Utils.Fill(stackedDerivativeMemory[i].inputMem, 0.0f);
            }

            //run forwardsand then run backwards backpropagating through recurring
            int dataIndex;
            for (dataIndex = 0; dataIndex < inputData.Length; dataIndex++)
            {
                Array.Copy(inputData[dataIndex], stackedRuntimeContext[dataIndex].inputData, stackedRuntimeContext[dataIndex].inputData.Length);
                neuralNetwork.Execute_FullContext(stackedRuntimeContext[dataIndex], stackedFullContext[dataIndex]);
                //neuralNetwork.ExecuteBackwards(targetData[dataArrayIndex][dataIndex], runtimeContext, fullContext, derivativeMemory);
            }
            //back propagate through stacked
            while (dataIndex-- > 0)
            {
                neuralNetwork.ExecuteBackwards(targetData[dataIndex], stackedRuntimeContext[dataIndex], stackedFullContext[dataIndex], stackedDerivativeMemory[dataIndex], 0, -1);
            }

            float[][] io = new float[inputData.Length][];
            for (int i = 0; i < io.Length; i++)
            {
                io[i] = new float[inputData[i].Length];
                Array.Copy(stackedDerivativeMemory[i].inputMem, io[i], io[i].Length);
            }
            return io;
        }

        /// <summary>
        /// Generate input deltas.
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="targetData"></param>
        /// <returns>Array of input deltas.</returns>
        public float[] InputErrorPropagation(float[] inputData, float[] targetData)
        {
            runtimeContext.Reset(true);
            derivativeMemory.Reset();
            Utils.Fill(derivativeMemory.inputMem, 0.0f);

            //simply run forwards and backwards
            Array.Copy(inputData, runtimeContext.inputData, runtimeContext.inputData.Length);
            neuralNetwork.Execute_FullContext(runtimeContext, fullContext);
            neuralNetwork.ExecuteBackwards(targetData, runtimeContext, fullContext, derivativeMemory, NeuralNetworkTrainer.LOSS_TYPE_CROSSENTROPY, -1);

            float[] io = new float[inputData.Length];
            Array.Copy(derivativeMemory.inputMem, io, io.Length);
            return io;
        }
    }
}
