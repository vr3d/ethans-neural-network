﻿//Ethan Alexander Shulman 2017

using System;
using System.Threading;
using System.IO;
using System.Collections.Generic;

namespace EthansNeuralNetwork
{

    /// <summary>
    /// Trains a NeuralNetwork through QLearning(action-reward system)
    /// </summary>
    public class NeuralNetworkQLearning
    {
        /// <summary>
        /// Max learning rate(0-1).
        /// </summary>
        public float maxLearningRate = 1e-1f;

        private bool hasRecurring;

        private NeuralNetwork neuralNetwork;

        private List<QLearningContext> rewardStack = new List<QLearningContext>();

        private NeuralNetworkContext[] stackedRuntimeContext;
        private NeuralNetworkFullContext[] stackedFullContext;
        private NeuralNetworkPropagationState[] stackedDerivativeMemory;
        private NeuralNetworkDerivativeMemory derivatives = new NeuralNetworkDerivativeMemory();

        /// <summary>
        /// AdaGrad memory, use this for saving/loading training state.
        /// </summary>
        public NeuralNetworkAdaGradMemory adagradMemory = new NeuralNetworkAdaGradMemory();

        private int maxUnrollLength;


        /// <summary>
        /// Create new NeuralNetworkQLearning system. 
        /// </summary>
        /// <param name="nn">NeuralNetwork to train.</param>
        /// <param name="inputDat">Input data.</param>
        /// <param name="targetDat">Target data.</param>
        public NeuralNetworkQLearning(NeuralNetwork nn, int maxUnrollLen)
        {
            neuralNetwork = nn;

            maxUnrollLength = maxUnrollLen;
            if (maxUnrollLength < 1) maxUnrollLength = 1;

            //check for recurring layer, if need to stack and unroll
            if (nn.outputLayer.recurring)
            {
                hasRecurring = true;
            }
            else
            {
                for (int i = 0; i < nn.hiddenLayers.Length; i++)
                {
                    if (nn.hiddenLayers[i].recurring)
                    {
                        hasRecurring = true;
                        break;
                    }
                }
            }

            derivatives.Setup(nn);
            adagradMemory.Setup(nn);
            adagradMemory.Reset();
            if (!hasRecurring)
            {
                maxUnrollLength = 1;
            }

            stackedRuntimeContext = new NeuralNetworkContext[maxUnrollLength];
            stackedFullContext = new NeuralNetworkFullContext[maxUnrollLength];
            stackedDerivativeMemory = new NeuralNetworkPropagationState[maxUnrollLength];
            for (int i = 0; i < stackedRuntimeContext.Length; i++)
            {
                stackedRuntimeContext[i] = new NeuralNetworkContext();
                stackedRuntimeContext[i].Setup(nn);

                stackedFullContext[i] = new NeuralNetworkFullContext();
                stackedFullContext[i].Setup(nn);

                stackedDerivativeMemory[i] = new NeuralNetworkPropagationState();
                stackedDerivativeMemory[i].Setup(nn, stackedRuntimeContext[i], stackedFullContext[i], derivatives);
            }
        }

        /// <summary>
        /// Prepares for learning.
        /// </summary>
        public void Start()
        {
            rewardStack.Clear();
        }


        //copies c1 recurring data to c2
        private void CopyRecurringState(NeuralNetworkContext c1, NeuralNetworkContext c2)
        {
            int i = c1.hiddenRecurringData.Length;
            while (i-- > 0)
            {
                if (c1.hiddenRecurringData[i] == null) continue;
                Array.Copy(c1.hiddenRecurringData[i], c2.hiddenRecurringData[i], c1.hiddenRecurringData[i].Length);
            }
        }


        
        public int Learn(float reward)
        {
            NeuralNetworkContext ctx = stackedRuntimeContext[0];
            float[] id = ctx.inputData;
            if (reward > 0.0f)
            {
                float[] ib = new float[id.Length];
                Array.Copy(id, ib, id.Length);

                //if any reward received, learn from last actions
                derivatives.Reset();
                for (int i = 0; i < maxUnrollLength; i++)
                {
                    stackedRuntimeContext[i].Reset(true);
                    stackedDerivativeMemory[i].Reset();
                }
                adagradMemory.learningRate = Math.Min(reward, maxLearningRate);


                float[] tb = new float[ctx.outputData.Length];
                Utils.Fill(tb, 0.0f);

                int alen = rewardStack.Count,
                    unrollCount = 0;
                for (int i = 0; i < alen; i++)
                {
                    QLearningContext qctx = rewardStack[i];

                    Array.Copy(qctx.input, stackedRuntimeContext[unrollCount].inputData, qctx.input.Length);
                    neuralNetwork.Execute_FullContext(stackedRuntimeContext[unrollCount], stackedFullContext[unrollCount]);

                    unrollCount++;
                    if (unrollCount >= maxUnrollLength || i+1 >= alen)
                    {
                        //back propagate through stacked
                        int tdatIndex = i;
                        while (unrollCount-- > 0)
                        {
                            qctx = rewardStack[tdatIndex];

                            tb[qctx.action] = 1.0f;
                            neuralNetwork.ExecuteBackwards(tb, stackedRuntimeContext[unrollCount], stackedFullContext[unrollCount], stackedDerivativeMemory[unrollCount], NeuralNetworkTrainer.LOSS_TYPE_AVERAGE, -1);
                            tb[qctx.action] = 0.0f;

                            tdatIndex--;
                        }

                        //learn
                        adagradMemory.Apply(stackedDerivativeMemory[0]);
                        derivatives.Reset();

                        unrollCount = 0;

                        if (i + maxUnrollLength >= alen)
                        {
                            //not enough room for another full length propagation
                        }
                        else
                        {
                            //copy recurring state over
                            CopyRecurringState(stackedRuntimeContext[maxUnrollLength - 1], stackedRuntimeContext[0]);
                        }
                    }
                    else
                    {
                        //copy recurring state into next
                        CopyRecurringState(stackedRuntimeContext[unrollCount - 1], stackedRuntimeContext[unrollCount]);
                    }
                }

                rewardStack.Clear();
                Array.Copy(ib, id, ib.Length);
            }

            neuralNetwork.Execute(ctx);
            //randomly select action from output result
            Utils.Normalize(ctx.outputData);
            int action = Utils.RandomChoice(ctx.outputData);
            rewardStack.Add(new QLearningContext(action, id));

            return action;
        }

        /// <summary>
        /// Get neural network runtime context.
        /// </summary>
        /// <returns></returns>
        public NeuralNetworkContext GetNeuralNetworkContext()
        {
            return stackedRuntimeContext[0];
        }

        /// <summary>
        /// Get AdaGrad memory, used for saving training state.
        /// </summary>
        /// <returns></returns>
        public NeuralNetworkAdaGradMemory GetAdaGradMemory()
        {
            return adagradMemory;
        }
    }


    public class QLearningContext
    {
        public int action;
        public float[] input;

        public QLearningContext(int a, float[] i)
        {
            action = a;
            input = new float[i.Length];
            Array.Copy(i, input, i.Length);
        }
    }
}
