//Ethan Alexander Shulman 2016

using System;
using System.IO;
using EthansNeuralNetwork;
using System.Collections.Generic;

namespace TextGenRNN
{
    public class TextGenRNN
    {

        public static void Main(string[] args)
        {

            //load example text
            string exampleText = File.ReadAllText("source.txt");
            List<char> dict;
            float[][] exampleTextEncoded =  Utils.EncodeStringOneHot(exampleText, out dict);

            //define neuralnetwork structure
            NeuralNetworkLayer inputLayer = new NeuralNetworkLayer(dict.Count, false, null);

            NeuralNetworkLayer[] hiddenLayers = new NeuralNetworkLayer[1];
            hiddenLayers[0] = new NeuralNetworkLayer(dict.Count, true, Utils.Tanh_ActivationFunction);

            NeuralNetworkLayer outputLayer = new NeuralNetworkLayer(dict.Count, false, Utils.Exp_ActivationFunction);

            NeuralNetwork neuralNetwork = new NeuralNetwork(inputLayer, hiddenLayers, outputLayer);


            //setup training
            float[][] trainingInput = new float[exampleTextEncoded.Length - 1][],
                      trainingTarget = new float[exampleTextEncoded.Length - 1][];
            Array.Copy(exampleTextEncoded, trainingInput, trainingInput.Length);
            Array.Copy(exampleTextEncoded, 1, trainingTarget, 0, trainingTarget.Length);


            NeuralNetworkTrainer trainer = new NeuralNetworkTrainer(neuralNetwork, new float[][][] { trainingInput }, new float[][][] { trainingTarget }, 25, true);
            trainer.desiredLoss = 0.01f;
            trainer.learningRate = 0.01f;

            trainer.StartInit();

            NeuralNetworkProgram nnp = new NeuralNetworkProgram(neuralNetwork);



            long lastTime = 0;

            while (trainer.GetCurrentLoss() > trainer.desiredLoss)
            {
                trainer.Learn();

                long nowTime = DateTime.Now.Ticks;
                if (nowTime - lastTime > 20 * 1000 * 10000)
                {
                    string stxt = "";
                    int ch = Utils.NextInt(0, dict.Count);

                    stxt += dict[ch];

                    nnp.ResetMemory(true);

                    for (int i = 0; i < 200; i++)
                    {
                        nnp.context.inputData[ch] = 1.0f;
                        nnp.Execute();
                        nnp.context.inputData[ch] = 0.0f;
                        
                        ch = Utils.RandomChoice(nnp.context.outputData);
                        stxt += dict[ch];
                    }

                    lastTime = nowTime;
                    Console.WriteLine("Loss: " + trainer.GetLoss() + ", iterations: " + trainer.GetIterations() + ", sample text: "+stxt);
                }
            }

            Console.ReadKey();

        }

    }
}
