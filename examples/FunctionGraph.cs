//Ethan Alexander Shulman 2016

using System;
using System.Threading;
using System.Drawing;
using EthansNeuralNetwork;

namespace MessingAround
{
    public class MessingAround
    {


        const bool ADAGRAD = true;
        const float TARGET_LOSS = 1e-2f;


        public static void Main(string[] args)
        {

            //create neuralnetwork structure
            NeuralNetworkLayer inputLayer = new NeuralNetworkLayer(1, false, null);

            NeuralNetworkLayer[] hiddenLayers = new NeuralNetworkLayer[1];
            hiddenLayers[0] = new NeuralNetworkLayer(32, false, Utils.Tanh_ActivationFunction);

            NeuralNetworkLayer outputLayer = new NeuralNetworkLayer(1, false, Utils.Identity_ActivationFunction);

            //create neuralnetwork
            NeuralNetwork neuralNetwork = new NeuralNetwork(inputLayer, hiddenLayers, outputLayer);
            if (ADAGRAD)
            {
                NeuralNetworkLayer.MIN_BIAS = 0.0f;
                NeuralNetworkLayer.MAX_BIAS = 0.0f;
                NeuralNetworkLayerConnection.MIN_WEIGHT = 0.0f;
                NeuralNetworkLayerConnection.MAX_WEIGHT = 1.0f;
            }
            else
            {
                NeuralNetworkLayer.MIN_BIAS = -1.0f;
                NeuralNetworkLayer.MAX_BIAS = 1.0f;
                NeuralNetworkLayerConnection.MIN_WEIGHT = -1.0f;
                NeuralNetworkLayerConnection.MAX_WEIGHT = 1.0f;
            }
            neuralNetwork.RandomizeWeightsAndBiases();

            //set up training data from actualFunction
            float[][] actualFunctionPlot = Utils.SampleFunction(100, actualFunction, 0.0f, 1.0f);
            float[][] trainingInput = new float[actualFunctionPlot.Length][],
                      trainingTarget = new float[actualFunctionPlot.Length][];
            int i = actualFunctionPlot.Length;
            while (i-- > 0)
            {
                float[] plot = actualFunctionPlot[i];
                trainingInput[i] = new float[] { plot[0] };
                trainingTarget[i] = new float[] { plot[1] };
            }


            //set up neuralnetwork training method

            NeuralNetworkEvolver evolver;
            NeuralNetworkTrainer trainer;

            if (ADAGRAD)
            {
                trainer = new NeuralNetworkTrainer(neuralNetwork, new float[][][] { trainingInput }, new float[][][] { trainingTarget }, 100, false);
                trainer.learningRate = 0.2f;
                trainer.learningRateDeclineScale = 1.0f;
                trainer.learningRateDecline = 0.25f;
                trainer.Start();
            }
            else
            {
                evolver = new NeuralNetworkEvolver(true, neuralNetwork, 1, trainingInput, trainingTarget);
                evolver.maxMutationRate = 0.25f;
                evolver.minMutationRate = 0.0f;
                evolver.mutationIncreaseRate = 0.0001f;
                evolver.Start();
            }

            //train and output status
            float loss = 1.0f;
            while (loss > TARGET_LOSS)
            {
                Thread.Sleep(1000);
                if (ADAGRAD)
                {
                    loss = trainer.GetLoss();
                    Console.WriteLine("Loss: " + loss + ", iterations: " + trainer.GetIterations());
                }
                else {
                    loss = evolver.GetLoss();
                    Console.WriteLine("Loss: " + loss + ", iterations: " + evolver.GetGenerations());
                }

            }


            //save image graph of actualfunction and neuralnetwork
            UtilsDrawing.DrawLineGraph(new float[][][] { actualFunctionPlot, Utils.SampleNeuralNetwork(100, neuralNetwork, 0.0f, 1.0f) }, new Color[] { Color.Red, Color.Blue }, new string[] { "actual function", "neural net" }, 0.0f, 1.0f, 0.0f, 1.0f).Save("graph.png");

            Console.ReadKey();
        }



        public static float actualFunction(float x)
        {
            float v = 0.0f;
            for (int i = 1; i < 4; i++)
            {
                v += (float)Math.Sin(i*2.39848f+(x*31.0f) / (float)i) * i;
            }
            return 0.45f+v/10.0f;
        }
    }
}