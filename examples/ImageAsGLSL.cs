//Ethan Alexander Shulman 2016

using System;
using System.Drawing;
using EthansNeuralNetwork;
using System.IO;

namespace ImageAsGLSL
{
    public class ImageAsGLSL
    {

        public const bool USE_ADAGRAD = true;

        public static void Main(string[] args)
        {

            Console.WriteLine("Starting image matching neural network...");

            //load target image
            Bitmap bmp = new Bitmap("image.png");

            //setup training data
            float[][] trainingInput = new float[bmp.Width * bmp.Height][],
                trainingTarget = new float[trainingInput.Length][];
            int tind = 0;
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    trainingInput[tind] = new float[] { x / (float)bmp.Width, y / (float)bmp.Height };

                    Color pxColor = bmp.GetPixel(x, y);
                    trainingTarget[tind] = new float[] { pxColor.R / 255f, pxColor.G / 255f, pxColor.B / 255f };
                    tind++;
                }
            }

            //setup neural network structure
            NeuralNetworkLayer inputLayer = new NeuralNetworkLayer(2, false, null);

            NeuralNetworkLayer[] hiddenLayers = new NeuralNetworkLayer[1];
            hiddenLayers[0] = new NeuralNetworkLayer(16, false, Utils.Rectifier_ActivationFunction);

            NeuralNetworkLayer outputLayer = new NeuralNetworkLayer(3, false, Utils.Rectifier_ActivationFunction);


            //neural network
            NeuralNetwork neuralNetwork = new NeuralNetwork(inputLayer, hiddenLayers, outputLayer);


            if (USE_ADAGRAD)
            {
                NeuralNetworkLayer.MIN_BIAS = 0.0f;
                NeuralNetworkLayer.MAX_BIAS = 0.0f;
                NeuralNetworkLayerConnection.MIN_WEIGHT = 0.0f;
                NeuralNetworkLayerConnection.MAX_WEIGHT = 0.01f;
                neuralNetwork.RandomizeWeightsAndBiases();

                //adagrad training
                NeuralNetworkTrainer trainer = new NeuralNetworkTrainer(neuralNetwork, new float[][][] { trainingInput }, new float[][][] { trainingTarget }, 1, NeuralNetworkTrainer.LOSS_TYPE_AVERAGE);
                trainer.desiredLoss = 0.01f;
                trainer.learningRate = 0.1f;
                trainer.lossSmoothing = 0.001f;

                trainer.shuffling = true;
                trainer.averageDerivs = false;

                //trainer.learningRateDecline = 0.9999f;
                trainer.learningRateDeclineStart = (long)1e10;
                trainer.learningRateDeclineEnd = (long)1e13;
                trainer.learningRateDeclinePower = 2.0f;

                trainer.Start();


                while (Console.ReadLine() != "done")
                {
                    Console.WriteLine("Loss: " + trainer.GetCurrentLoss() + ", iterations: " + trainer.GetIterations());
                }
                trainer.Stop();
                Console.WriteLine("Loss: " + trainer.GetCurrentLoss() + ", iterations: " + trainer.GetIterations());
            }
            else
            {
                NeuralNetworkLayer.MIN_BIAS = -1000.0f;
                NeuralNetworkLayer.MAX_BIAS = 1000.0f;
                NeuralNetworkLayerConnection.MIN_WEIGHT = -10.0f;
                NeuralNetworkLayerConnection.MAX_WEIGHT = 10.0f;
                neuralNetwork.RandomizeWeightsAndBiases();

                //evolution training
                NeuralNetworkEvolver evolver = new NeuralNetworkEvolver(true, neuralNetwork, 8, trainingInput, trainingTarget);
                evolver.desiredLoss = 0.04f;
                evolver.maxMutationRate = 0.95f;
                evolver.mutationIncreaseRate = 0.01f;
                evolver.Start();

                while (Console.ReadLine() != "done")
                {
                    Console.WriteLine("Loss: " + evolver.GetLoss() + ", iterations: " + evolver.GetGenerations());
                }
                evolver.Stop();
                neuralNetwork = evolver.GetBestNeuralNetwork();
                Console.WriteLine("Loss: " + evolver.GetLoss() + ", iterations: " + evolver.GetGenerations());
            }

            //save neuralnetwork as glsl
            File.WriteAllText("glslimage.txt", Utils.AsGLSL(neuralNetwork));

            //save neuralnetwork image
            Utils.AsImage(neuralNetwork, bmp.Width, bmp.Height).Save("render.png");

            Console.WriteLine("Done.");
            Console.ReadKey();
        }

    }
}